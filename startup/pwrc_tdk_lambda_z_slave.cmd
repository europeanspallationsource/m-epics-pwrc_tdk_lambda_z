# @field DEVICENAME
# @type STRING
# Device name, should be the same as the name in CCDB

# @field RS485_ADDR
# @type INTEGER
# The RS485 address

# @field MASTER
# @type STRING
# The device name of the master PS


#-Load the database defining your EPICS records
dbLoadRecords(pwrc_tdk_lambda_z_slave.db, "P = $(DEVICENAME), R = :, ASYNPORT = $(MASTER), RS485_ADDR = $(RS485_ADDR), MASTER = $(MASTER)")
